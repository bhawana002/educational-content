# NMAT 2020 exam Registration and Important Dates
Graduate Management Admission Council (GMAC) has announced the [NMAT 2020 Exam](https://www.mbarendezvous.com/nmat/) dates. The exam pattern for NMAT 2020 has also been revised. The NMAT exam is conducted over a period of 75 days. There are five windows of 15 days each:

1. NMAT Window 1

2. NMAT Window 2

3. NMAT Window 3

4. NMAT Window 4

5. NMAT Window 4

NMAT by GMAC exam can be self-scheduled only after completing NMAT registration and paying the exam fee. All important dates related to NMAT exam are:

**NMAT Registration Dates**

1. Exam Registration Begins – Thursday, 2nd July 2020 (INR 2000 applicable taxes)

2. NMAT Registration Last Date – Monday, 5th Oct 2020

3. NMAT Late Registration Window – Tuesday, 6th Oct till 16th Oct 2020 (INR 2500 applicable taxes)

4. [NMAT 2020 Registration](https://www.mbarendezvous.com/nmat/nmat-registration/) - Retake – Wednesday, 7th Oct till Wednesday, 16th Dec 2020 (INR 2000 applicable taxes)

**NMAT Exam Dates**

1. Exam Days – Tuesday, 6th Oct till Saturday, 19th Dec 2020

2. NMAT Window 1 – 6th Oct to 20th Oct

3. NMAT Window 2 – 21st Oct to 4th Nov

4. NMAT Window 3 – 5th Nov to 19th Nov

5. NMAT Window 4 – 20th Nov to 4th Dec

6. NMAT Window 5 – 5th Dec to 19th Dec

**NMAT Scheduling Dates**

1. NMAT 2020 Scheduling – Thursday, 2nd July till 18th Oct 2020

2. NMAT Re-Scheduling Window – Thursday, 2nd July till 16th Dec 2020 (INR 1100 applicable taxes)

3. NMAT Retake Scheduling – Wednesday, 7th Oct till 16th Dec 2020

**NMAT Exam Registration Process**

1. Create and register an account on NMAT official website.

2. Login using registered credentials.

3. Fill the NMAT by GMAC form with all personal, academic and contact details. Upload photograph and signature.

4. Choose 5 schools of preference. The score report (INR 200 applicable taxes) will automatically be sent to the chosen colleges.

5. Make payment using - Visa or MasterCard - Credit Card, Debit Card, Net Banking, American Express, Diners Club or Mobile Wallets.

*Note: Candidates can retake NMAT by GMAC 2020 exam twice after the first attempt. Thus, they can appear for the exam thrice. There is separate fee for late registration, registration-retake, rescheduling and retake-scheduling.*